package com.oehm5;

public class Student {
	private Long id;
	private String name;
	private Long contactNum;
	private Long age;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getContactNum() {
		return contactNum;
	}
	public void setContactNum(Long contactNum) {
		this.contactNum = contactNum;
	}
	public Long getAge() {
		return age;
	}
	public void setAge(Long age) {
		this.age = age;
	}
	
	public String toString() {
		return "Student details are \n"+name+"\n"+age+"\n"+contactNum+"\n";
	}
}
