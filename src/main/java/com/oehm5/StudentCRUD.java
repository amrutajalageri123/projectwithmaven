package com.oehm5;

import org.hibernate.Session;

public class StudentCRUD 
{
	public void saveStudentDetails(Student student)
	{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(student);
		session.getTransaction().commit();
	}
	
	public Student getStudentById(Long id)
	{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Student student = session.get(Student.class, id);
		return student;
	}
	
	public void updateStudentbyId(Long id,String name)
	{
		Student student = getStudentById(id);
		if(student!=null)
		{
		student.setName(name);
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(student);
		session.getTransaction().commit();
		return;
		}
	}
	
	public void deleteStudentDetails(Long id)
	{
		Student student = getStudentById(id);
		if(student!=null)
		{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(student);
		session.getTransaction().commit();
		return;
		}
	}
}
