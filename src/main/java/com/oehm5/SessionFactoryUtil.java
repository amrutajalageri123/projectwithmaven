package com.oehm5;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryUtil 
	{
		private static SessionFactory session=null;
		private SessionFactoryUtil() {
		}
		
		public static SessionFactory getSessionFactory() {
			if(session==null)
				session=new Configuration().configure().buildSessionFactory();
			return session;
		}
}

